#ifndef _MASSANDFORMULA_H_
#define _MASSANDFORMULA_H_

std::string IntToString ( int number );

void massAndFormula( const string smile, float &mass, string &formula );

#include "massAndFormula.inl"

#endif
