#include "ListNodeClass.h"
#include "QueueClass.h"
#include "UnitClass.h"

using namespace std;
template < class T >


// default constructor
QueueClass< T >::QueueClass()
{
  head = NULL;
  tail = NULL;
}

 //Copy constructor.  Will make a complete copy of the list, such
    //that one can be changed without affecting the other
template < class T >
QueueClass< T >::QueueClass(
         const QueueClass< T > &rhs
         )
{
  ListNodeClass< T > *newNode;
  if(rhs.head == NULL)
  {
    cout << "No Head Pointer for Copy Constructor to Begin" << endl;
  }
  else
  {
    ListNodeClass< T > *temp = rhs.head;
    newNode = new ListNodeClass< T >(NULL, temp->getValue(),
                                   NULL);
    head = newNode;
    newNode->setBeforeAndAfterPointers();
    cout << "Copy of " << newNode->getValue() << " is stored in " << newNode << endl;
    temp = temp->getNext();

    while(temp != NULL)
    {
      newNode = new ListNodeClass< T >(newNode, temp->getValue(),
                                       NULL);
      cout << "Copy of " << newNode->getValue() << " is stored in " << newNode << endl;
      newNode->setBeforeAndAfterPointers();
      temp = temp->getNext();
    }
    tail = newNode;
  }
}

template < class T >
    //Clears the list to an empty state without resulting in any
    //memory leaks.
void QueueClass< T >::clear(
         )
{
  ListNodeClass< T > *temp = head;
  while(temp != NULL)
  {
    temp = temp->getNext();
    if(temp != NULL)
    {
      delete temp->getPrev();
    }
  }
  if(head != NULL)
  {
    head = NULL;
  }
  if(tail != NULL)
  {
    tail = NULL;
  }
}

// insert a new element according to the ascending order
template < class T >
void QueueClass< T >::insertValue(
         const T &valToInsert  //The value to insert into the list
         )
{
  bool success = false;
  T thisValue;
  ListNodeClass< T > *nodePtr = head;
  ListNodeClass< T > *newNode = NULL;

  // initiate the linked list if the list is empty
  if ( nodePtr == NULL )
  {
    newNode = new ListNodeClass< T >( NULL,
                                      valToInsert,
                                      NULL );
    head = tail = newNode;
  }
  // add the the existing linked list
  else
  {
    while ( nodePtr != NULL && !success )
    {
    thisValue = nodePtr->getValue();

      // if the incoming value is smallest, insert at the head
      if ( valToInsert < thisValue )
      {
        newNode = new ListNodeClass< T >( nodePtr->getPrev(),
                                          valToInsert,
                                          nodePtr );
        if ( nodePtr->getPrev() == NULL )
        {
          head = newNode;
        }
        newNode->setBeforeAndAfterPointers();
        success = true;
      }
      // if the incoming value is medium, insert in the middle
      else
      {
        // if incoming value is largest, inserts at the end
        if ( nodePtr->getNext() == NULL && !success )
        {
          newNode = new ListNodeClass< T >( tail,
                                            valToInsert,
                                            NULL );
          newNode->setBeforeAndAfterPointers();
          tail = newNode;
          success = true;
        }
        nodePtr = nodePtr->getNext();
      }
    }
  }
}




template < class T >
    // QueueClass inserts nodes at the head and allows them
    // to be retrieved there. Later nodes are added farthe
    // away from the head to ensure that earlier in means
    // earlier out.
void QueueClass< T >::enqueue(
         const T &valToInsert  //The value to insert into the list
         )
{
  bool insert = false;
  ListNodeClass< T > *newNode;
  if(tail == NULL)
  {
    newNode = new ListNodeClass< T >(NULL, valToInsert, tail);
  }
  else
  {
    newNode = new ListNodeClass< T >(tail, valToInsert, NULL);
  }
  if(head == NULL)
  {
    head = newNode;
  }
    newNode->setBeforeAndAfterPointers();
    tail = newNode;
    insert = true;
}

template < class T >
    //Prints the contents of the list from head to tail to the screen.
    //Begins with a line reading "Forward List Contents Follow:", then
    //prints one list element per line, indented two spaces, then prints
    //the line "End Of List Contents" to indicate the end of the list.
void QueueClass< T >::printForward(
         ) const
{
  cout << endl;
  ListNodeClass< T > *temp = head;
  if(temp == NULL)
  {
    cout << "Can't Print Fowards: List is Empty" << endl;
  }
  else
  {
    while(temp != NULL)
    {
      cout << "  " << temp->getValue();
      temp = temp->getNext();
    }
  }
  cout << "printing" << endl;
}

template < class T >
    //Prints the contents of the list from tail to head to the screen.
    //Begins with a line reading "Backward List Contents Follow:", then
    //prints one list element per line, indented two spaces, then prints
    //the line "End Of List Contents" to indicate the end of the list.
void QueueClass< T >::printBackward(
         ) const
{
  cout << "Backward List Contents Follow: " << endl;
  ListNodeClass< T > *temp = tail;
  if(temp == NULL)
  {
    cout << "Can't Print Backwards: List is Empty" << endl;
  }
  else
  {
    while(temp != NULL)
    {
      cout << "  " << temp->getValue() << endl;
      temp = temp->getPrev();
    }
    cout << "End of List Contents" << endl;
  }
}


template< class T >
    //Removes the front item from the list and returns the value that
    //was contained in it via the reference parameter.  If the list
    //was empty, the function returns false to indicate failure, and
    //the contents of the reference parameter upon return is undefined.
    //If the list was not empty and the first item was successfully
    //removed, true is returned, and the reference parameter will
    //be set to the item that was removed.
bool QueueClass< T >::dequeue(
         T &theVal
         )
{
  bool status = true;
  ListNodeClass< T > *temp = head;
  if(temp == NULL)
  {
    cout << "Can't Delete from List: No Head"
         << endl;
    status = false;
  }
  else
  {
    theVal = temp->getValue();
    head = temp->getNext();
    if(temp->getNext() != NULL)
    {
      temp->getNext()->setPreviousPointer(NULL);
      delete temp;
    }
    else
    {
      tail = NULL;
    }
  }
  return status;
}

template < class T >
    //Returns the number of nodes contained in the list.
int QueueClass< T >::getNumElems(
         ) const
{
  int nodeCount = 0;
  ListNodeClass< T > *temp = head;
  if(temp == NULL)
  {}
  else
  {
    while(temp != NULL)
    {
      temp = temp->getNext();
      nodeCount++;
    }
  }

  return nodeCount;
}

template < class T >
    //Provides the value stored in the node at index provided in the
    //"index" parameter.  If the index is out of range, then outVal
    //remains unchanged and false is returned.  Otherwise, the function
    //returns true, and the reference parameter outVal will contain
    //a copy of the value at that location.
T QueueClass< T >::getElemAtIndex( const int index )
{
  T outVal;
  int objectNum = 0;
  bool success = false;
  ListNodeClass< T > *node = head;

  if ( node == NULL )
  {
    cout << "List is empty." << endl;
    success = false;
  }
  else
  {
    // keep moving along the list until index element reached
    while ( node != NULL || !success )
    {
      if ( objectNum == index )
      {
        outVal = node->getValue();
        success = true;
      }
      else
      {
        node = node->getNext();
      }
      objectNum++;
    }
  }
  return ( outVal );
}


template < class T >
string QueueClass< T >::preAssembly( )
{
  string peptide;
  for ( int i = 0; i < getNumElems(); i++ )
  {
    UnitClass *temp = new UnitClass;
    *temp = getElemAtIndex( i );
    peptide = peptide + temp->getSmile();
    delete temp;
  }
  return ( peptide );
}
