#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#include <string>
using namespace std;


// menu options
const int MENU_NUM = 7;
const string MENU[MENU_NUM] = { "Input NRPS/PKS. ",
                                "Input postassembly modifications. ",
                                "Filter NRPS products by mass. ",
                                "Print NRPS residues. ",
                                "Print NRPS library!",
                                "Output NRPS into file.",
                                "Exit."
                              };
const int INPUT_RES = 1;
const int INPUT_POSTMODS = 2;
const int INPUT_MS = 3;
const int PRINT_NRPS = 4;
const int GENERATE = 5;
const int OUTPUT = 6;
const int EXIT = 7;

// supported NRPS residues
const string RESID_DATABASE = "aaSMILES.txt";
const string INITI_DATABASE = "startingAminoAcid.txt";
const int MIN_RESID_NUM = 1;
const int MAX_RESID_NUM = 32;
const string RESIDUES = "ALA ARG ASN ASP CYS GLN GLU GLY HIS ILE LEU LYS \
                         PHE PRO SER THR TRP TYR VAL MET \
                         ala arg asn asp cys gln glu gly his ile leu lys \
                         phe pro ser thr trp tyr val met\
                         Ala Arg Asn Asp Cys Gln Glu Gly His Ile Leu Lys \
                         Phe Pro Ser Thr Trp Tyr Val Met\
                         MPRO 23DHB 34DHB 2HIVA ORN PGLY DAB BALA AEO 4MHA \
                         PICO AAA DHA SCY PIP BMT ADDS \
                         mpro 23dhb 34dhb 2hiva orn pgly dab b-Ala aeo 4mha \
                         pico aaa dha scy pip bmt adds \
                         Mpro 23Dhb 34Dhb 2Hiva Orn Pgly Dab Bala Aeo 4Mha \
                         Pico Aaa Dha Scy Pip Bmt Adds \
                         mal mmal omal emal nrp pk aad mpro dhb phg abu \
                         hiv dhpg DHpg bht 3-me-glu 4pPro ala-b ala-d \
                         dht Sal tcl blys hpg hyv-d iva vol mxmal";

// supported NRPS modification domains
const int MIN_MODUL_NUM = 1;
const int MAX_MODUL_NUM = 10;
const string MODULES = "ME OME SME NME EPI HYD EPOX OX I CL BR CY ES TE RE GLY\
                        Me OMe Sme NMe Epi Hyd Epox Ox I Cl Br Cy Es Te Re Gly\
                        me ome sme nme epi hyd epox ox i cl br cy es te re gly\
                        ";


// atomic weight of elements
const double N = 14.00674;
const double C = 12.0107;
const double H = 1.00794;
const double O = 15.9994;
const double S = 32.066;
const double Cl = 35.4527;
const double Br = 79.904;
const double I = 126.90447;

// Mass-spec limits
const float MS_MIN = 0.0;
const float MS_MAX = 1000000;

#endif
